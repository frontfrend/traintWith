var gulp = require('gulp');
var WebpackDevServer = require('webpack-dev-server');
var webpackStream = require('webpack-stream');
var webpack = require('webpack');
var gutil = require('gutil');


var webpackConfig = require('./webpack.config.dev.js');

gulp.task('dev', function() {
  return gulp.src('app/entry.tsx')
    .pipe(webpackStream( require('./webpack.config.dev.js') ))
    .pipe(gulp.dest('dist/'));
});


gulp.task("webpack-dev-server", function(callback) {
    // Start a webpack-dev-server
    var compiler = webpack(webpackConfig);

    new WebpackDevServer(compiler, {
      hot: true,

      stats: {
			colors: true
		}
        // server and middleware options
    }).listen(8080, "localhost", function(err) {
        if(err) throw new gutil.PluginError("webpack-dev-server", err);
        // Server listening
        gutil.log("[webpack-dev-server]", "http://localhost:8080/webpack-dev-server/index.html");

        // keep the server alive or continue?
        // callback();
    });
});
