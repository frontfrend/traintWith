const path = require('path');
const webpack = require('webpack');

var HtmlWebpackPlugin = require('html-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
  mode: 'development',
  devtool: "source-map",
  context: path.resolve(__dirname),
  entry: [ "react-hot-loader/webpack", './app/entry.tsx'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.bundle.js'
  },
  resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [{
            loader: "style-loader"
        }, {
            loader: "css-loader"
        }, {
            loader: "sass-loader",
        }]
      },
      {
        test: /\.(ts|tsx)$/,
        exclude: /(node_modules|bower_components)/,
        include: path.resolve(__dirname, "app"),
        use: [
          'ts-loader', // (or awesome-typescript-loader)
          {
            loader: 'babel-loader',
            options: {
              plugins: [
                '@babel/plugin-syntax-typescript',
                '@babel/plugin-syntax-decorators',
                '@babel/plugin-syntax-jsx',
                'react-hot-loader/babel',
              ],
            },
          }
        ]
      },
      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
    ]
  },
  externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
  plugins: [
    new HtmlWebpackPlugin({template: 'index.html', inject: 'body'}),
    new TsconfigPathsPlugin({ configFile: "./tsconfig.json"})
  ],
  devServer: {
        hot: true
    }
};
