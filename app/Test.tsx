import * as React from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from './redux/actions';

export interface HelloProps {
  count: number;
  increment: any
}

export class Test extends React.Component<HelloProps, {}> {
  render() {
          return (<div className="root">
            <h1>Counter 222</h1>
            <button onClick={this.props.increment}>increase</button>
            {this.props.count}
          </div>);
      }
}

const mapStateToProps = state => ({
  count: state.increment
})

const mapDispatchToProps = dispatch => ({
    increment: bindActionCreators(actions.increment.increment, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Test)
