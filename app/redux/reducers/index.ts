import { combineReducers } from 'redux';
import increment from './increment';

const appData = combineReducers({increment});

export default appData;
