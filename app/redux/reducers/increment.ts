import constants from '../constants';

export const increment = (state = 0, action) => {
    switch (action.type) {
      case constants.increment.INCREMENT:
        return state + 1;
      default: return state;
    }
  };

export default increment;
