import { createStore, StoreEnhancer} from 'redux';
import appData from './reducers'

const getStore = (initState={}) => {
  return createStore(appData, initState,
      (<any>window).__REDUX_DEVTOOLS_EXTENSION__ && (<any>window).__REDUX_DEVTOOLS_EXTENSION__()
    );
}

export default getStore;
