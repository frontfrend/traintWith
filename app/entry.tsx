import * as React from "react";
import * as ReactDOM from "react-dom";
import Test from "./Test";
import { AppContainer } from "react-hot-loader";
import { Provider } from 'react-redux'
import getStore from './redux';
require('./root.scss');
const rootEl = document.getElementById("root");

let store = getStore()
ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      <Test />
    </Provider>
  </AppContainer>,
    rootEl
);

if (module.hot) {
  module.hot.accept("./Test", () => {
    const NextApp = require("./Test").default;
    return ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <NextApp />
        </Provider>
      </AppContainer>,
      rootEl);
  });
}
